package root;

import root.Geometry.*;
import root.Model.Model;
import root.Model.NamedModel;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import static java.lang.Math.abs;

enum LIGHT_POS {
    LIGHT_UP,
    LIGHT_DOWN,
    LIGHT_STD
}

public class ivDisplay extends JPanel {
    final public static Double INF = Math.pow(10, 6);
    final public static int LIGHT_TO_UP = 1;
    final public static int LIGHT_TO_DOWN = 2;
    final public static int LIGHT_TO_STD = 3;

    private ArrayList<Triangle> allTriangle;
    private ArrayList<DrawableShape> all2DShape;
    private Graphics2D graphics;
    private Matrix3 transform;

    private LIGHT_POS lightPos;
    private Vertex lightPoint;

    private double powerLight;
    private int backgroundColor;

    public ivDisplay() {
        allTriangle = new ArrayList<>();
        all2DShape = new ArrayList<>();
        powerLight = 1.0;
        lightMoveTo(LIGHT_TO_STD);
        backgroundColor = 0;
    }

    public void setBackgroundColor(int color) {
        if (color < 0)
            color = 0;
        if (color > 255)
            color = 255;
        backgroundColor = color;
    }

//    public void setWidthControlPanel(int w) {
//        widthControlPanel = w;
//    }

    public void lightMoveTo(Integer to) {

        switch (to) {
            case LIGHT_TO_DOWN:
                lightPos =  LIGHT_POS.LIGHT_DOWN;
                break;
            case LIGHT_TO_UP:
                lightPos = LIGHT_POS.LIGHT_UP;
                break;
            case LIGHT_TO_STD:
                lightPos = LIGHT_POS.LIGHT_STD;
                break;
        }
        updateLightPoint();
    }
    private void updateLightPoint() {
        switch (lightPos) {
            case LIGHT_STD:
                lightPoint = new Vertex(0, 0, -INF);
                break;
            case LIGHT_UP:
                lightPoint = new Vertex(0, -INF, 0);
                break;
            case LIGHT_DOWN:
                lightPoint = new Vertex(0, INF, 0);
                break;
            default:
                lightPoint = new Vertex(0,0,0);
        }
    }

    public void setPowerLight(double light) {
        powerLight = light;
    }

    // Отрисовка элементов (repaint)
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        graphics = (Graphics2D)g;
        graphics.setColor(new Color(backgroundColor, backgroundColor, backgroundColor));
        graphics.fillRect(0,0,getWidth(), getHeight());

        // RENDER
        BufferedImage img = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
        int n = img.getWidth() * img.getHeight();
        double zBuffer[] = new double[n];
        for (int i = 0; i < n; i++)
            zBuffer[i] = Double.NEGATIVE_INFINITY;

        Vertex drawLightPos = lightPos != LIGHT_POS.LIGHT_STD ? transform.transform(lightPoint) : lightPoint;
        // Подсчет нормалей
        for (Triangle t : allTriangle) {
            Vertex drawV1 = transform.transform(t.getV1());
            Vertex drawV2 = transform.transform(t.getV2());
            Vertex drawV3 = transform.transform(t.getV3());
            // Перенос в центр экрана
            drawV1.moveOn(getWidth()/2, getHeight()/2, 0);
            drawV2.moveOn(getWidth()/2, getHeight()/2, 0);
            drawV3.moveOn(getWidth()/2, getHeight()/2, 0);

            Vector normal = new Triangle(drawV1, drawV2, drawV3).getNormal();
            normal.normalize();

            Vector light = new Vector(drawLightPos, new Vertex(t.getCenter()));
            light.normalize();

            // Считаем косинус укла между направлением света и гранью.
            // Ни на что не делим, потому что вектора нормализованы
            double angleCos = normal.x()*light.x() + normal.y()*light.y() + normal.z()*light.z();
            int color = shade(t.getColor(), angleCos).getRGB();

            // Какую часть экрана (прямоугольник) я буду обновлять
            int minX = (int)Math.max(0, Math.min(drawV1.x(), Math.min(drawV2.x(), drawV3.x())));
            int maxX = (int)Math.min(img.getWidth()-1, Math.max(drawV1.x(), Math.max(drawV2.x(), drawV3.x())));
            int minY = (int)Math.max(0, Math.min(drawV1.y(), Math.min(drawV2.y(), drawV3.y())));
            int maxY = (int)Math.min(img.getHeight()-1, Math.max(drawV1.y(), Math.max(drawV2.y(), drawV3.y())));

            double dX_23 = drawV2.x() - drawV3.x();
            double dX_31 = drawV3.x() - drawV1.x();
            double dX_12 = drawV1.x() - drawV2.x();

            double dY_23 = drawV2.y() - drawV3.y();
            double dY_31 = drawV3.y() - drawV1.y();
            double dY_12 = drawV1.y() - drawV2.y();

            double triangleArea = (drawV1.y() - drawV3.y())*(drawV2.x() - drawV3.x()) + (drawV2.y() - drawV3.y())*(drawV3.x() - drawV1.x());
            for (int y = minY; y <= maxY; y++)
                for (int x = minX; x <= maxX; x++) {
                    double b1 = ((y - drawV3.y()) * dX_23 + dY_23 * (drawV3.x() - x)) / triangleArea;
                    if (b1 >= 0 && b1 <= 1) {
                        double b2 = ((y - drawV1.y()) * dX_31 + dY_31 * (drawV1.x() - x)) / triangleArea;
                        if (b2 >= 0 && b2 <= 1) {
                            double b3 = ((y - drawV2.y()) * dX_12 + dY_12 * (drawV2.x() - x)) / triangleArea;
                            if (b3 >= 0 && b3 <= 1) {
                                // Считаем глубину
                                double depth = b1 * drawV1.z() + b2 * drawV2.z() + b3 * drawV3.z();
                                int zIndex = y * img.getWidth() + x;
                                if (zBuffer[zIndex] < depth) {
                                    img.setRGB(x, y, color);
                                    zBuffer[zIndex] = depth;
                                }
                            }
                        }
                    }
                }
            graphics.drawImage(img, 0, 0, null);

            // Рисуем 2D фигурки
//            for (DrawableShape shape : all2DShape)
//                shape.draw(this);
        }
    }

    public void drawLine(Vertex v1, Vertex v2) {
        drawLine(v1, v2, Color.WHITE);
    }

    public void drawLine(Vertex v1, Vertex v2, Color color) {
        graphics.setColor(color);
        graphics.drawLine(
                (int)v1.x(),
                (int)v1.y(),
                (int)v2.x(),
                (int)v2.y()
        );
    }

    public void drawCircle(Vertex center, int radius) {
        graphics.setColor(Color.WHITE);
        graphics.drawOval(
                (int)center.x()-radius,
                (int)center.y()-radius,
                radius*2,
                radius*2
        );
    }

    public void drawLine(Line l) {
        drawLine(new Vertex(l.getX1(), l.getY1(), 0), new Vertex(l.getX2(), l.getY2(), 0));
    }

    public void addModel(Model m) {
        for (Triangle t : m.getTriangle())
            allTriangle.add(t);
    }

    public void add2DShape(DrawableShape shape) {
        all2DShape.add(shape);
    }


    public void addLine(Vertex v1, Vertex v2) {
        add2DShape(new Line(v1, v2));
    }

    public void addLine(double x1,double y1, double x2, double y2) {
        add2DShape(new Line(x1, y1, x2,y2));
    }

    public void reload(ArrayList<NamedModel> model) {
        allTriangle.clear();
        for (Model m : model)
            addModel(m);
    }
    public void removeModel(Model model) {
        for (Triangle t : model.getTriangle())
            allTriangle.remove(t);
    }

    public void removeAll() {
        allTriangle.clear();
    }

    public void updateTransform(Camera camera) {
        Matrix3 horizontalRotate = Matrix3.rotationY(camera.getHorizontalAngle());
        Matrix3 verticalTransform = Matrix3.rotationX(camera.getVerticalAngle());
        transform = horizontalRotate.multiply(verticalTransform);
    }

    public Color shade(Color color, double cos) {
        if (cos > 0)
            cos = 0;
        cos *= powerLight;
        if (cos < -1)
            cos = -1;

        cos = abs(cos);
        int red = (int)(color.getRed() * cos);
        int green = (int)(color.getGreen() * cos);
        int blue = (int)(color.getBlue() * cos);
        return new Color(red, green, blue);
    }
}