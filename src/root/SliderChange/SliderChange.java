package root.SliderChange;

import root.Engine.EngineAPI;
import root.ivDisplay;

import javax.swing.event.ChangeListener;

abstract public class SliderChange implements ChangeListener {
    protected EngineAPI engineAPI;
    protected ivDisplay display;
    public SliderChange(EngineAPI api, ivDisplay display) {
        this.engineAPI = api;
        this.display = display;

    }
}
