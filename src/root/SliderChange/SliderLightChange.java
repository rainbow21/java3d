package root.SliderChange;

import root.Engine.EngineAPI;
import root.ivDisplay;

import javax.swing.*;
import javax.swing.event.ChangeEvent;

public class SliderLightChange extends SliderChange {
    public SliderLightChange(EngineAPI api, ivDisplay display) {
        super(api,display);
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        JSlider slider = (JSlider) e.getSource();
        display.setPowerLight(1.0 + 1.0 / slider.getMaximum() * slider.getValue());

        display.setBackgroundColor(255/(slider.getMaximum()-slider.getMinimum()) * (slider.getValue()-slider.getMinimum()));
        display.repaint();
    }
}
