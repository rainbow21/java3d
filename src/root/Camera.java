package root;

public class Camera {
    double horizontalAngle;
    double verticalAngle;

    public double getHorizontalAngle() {
        return horizontalAngle;
    }

    public void setHorizontalAngle(double horizontalAngle) {
        this.horizontalAngle = Math.toRadians(horizontalAngle);
    }

    public double getVerticalAngle() {
        return verticalAngle;
    }

    public void setVerticalAngle(double verticalAngle) {
        this.verticalAngle = Math.toRadians(verticalAngle);
    }
}
