package root.Engine;

import root.Camera;
import root.GUI.*;
import root.Model.Builder;
import root.Model.Manager;
import root.Model.Model;
import root.Model.NamedModel;
import root.ivDisplay;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;

public class Engine implements ChangeListener, EngineAPI{

    private String TYPE_BOX = "Куб";
    private String TYPE_TETRAHEDR = "Тетраэдр";
    private String TYPE_CYLINDER = "Цилиндр";
    private String TYPE_SPHERE = "Сфера";

    private int WIDTH = 700, HEIGHT = 700;

    private JFrame frame;           // Окно
    private JSlider headingSlider;  // Ориентация по горизонтали
    private JSlider pitchSlider;    // Ориентация по вертикали
    private ControlPanel controlPanel; // Панель управления
    private JPanel appendPanel;
    private ivDisplay display;      // Панель, для рисования
    private Color selectColor = Color.GRAY;      // Цвет выделения
    private Manager modelManager = new Manager();
    private NamedModel currentModel; // Текущая модель
    private Camera camera;

    public void start() {
        MainWindow wnd = new MainWindow();

        frame = new JFrame();
        frame.setContentPane(wnd.$$$getRootComponent$$$());
        // Ставим слайдеры по краям
        headingSlider = wnd.getHorizontalSlider();
        headingSlider.addChangeListener(this);
        pitchSlider = wnd.getVerticalSlider();
        pitchSlider.addChangeListener(this);

        display = new ivDisplay();
        camera = new Camera();
        camera.setHorizontalAngle(headingSlider.getValue());
        camera.setVerticalAngle(pitchSlider.getValue());
        display.updateTransform(camera);
        display.setLayout(new BorderLayout());
        display.addMouseListener(new DisplayClick(this, display));
        wnd.getRoot().add(display, BorderLayout.CENTER);

        appendPanel = new JPanel(new BorderLayout());
        appendPanel.setVisible(false);
        display.add(appendPanel, BorderLayout.EAST);

        createTestScene();
        // Располагаем панель управления
        controlPanel = new ControlPanel(this, display);
        wnd.getRoot().add(controlPanel.$$$getRootComponent$$$(), BorderLayout.WEST);
        update();

        fillModelTypeBox();

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setSize(WIDTH, HEIGHT);
        frame.setExtendedState(Frame.MAXIMIZED_BOTH);
    }

    private void fillModelTypeBox() {
        JComboBox comboBox = controlPanel.getBoxAppendModel();
        comboBox.removeAllItems();
        comboBox.addItem(TYPE_BOX);
        comboBox.addItem(TYPE_TETRAHEDR);
        comboBox.addItem(TYPE_CYLINDER);
        comboBox.addItem(TYPE_SPHERE);
    }

    private void createTestScene() {
        int SIZE = 100;
        currentModel = Builder.buildSphere("Сфера0", SIZE,  0);
        currentModel.moveLeft(360);
        appendModel(currentModel);

        currentModel = Builder.buildSphere("Сфера1", SIZE,1);
        currentModel.moveLeft(120);
        appendModel(currentModel);

        currentModel = Builder.buildSphere("Сфера2", SIZE,2);
        currentModel.moveRight(120);
        appendModel(currentModel);

        currentModel = Builder.buildSphere("Сфера3", SIZE,3);
        currentModel.moveRight(470);
        appendModel(currentModel);
    }


    @Override
    public void clearScene() {
        display.removeAll();
        modelManager.removeAllModel();
        currentModel = null;
        updateComboBox();
    }

    // Реакция на изменение слайдеров
    @Override
    public void stateChanged(ChangeEvent e) {
        // Горизонтальное вращение
        if (e.getSource() == headingSlider) {
            camera.setHorizontalAngle(headingSlider.getValue());
        } else if (e.getSource() == pitchSlider) {
            camera.setVerticalAngle(pitchSlider.getValue());
        }
        display.updateTransform(camera);
        display.repaint();
    }

    @Override
    public void update() {
        updateComboBox();
        updateViewSelected();
    }

    @Override
    public void updateComboBox() {
        if (controlPanel == null)
            return;
        if (modelManager.noModels())
            controlPanel.disableAll();
        else {
            JComboBox comboBox = controlPanel.getSelectModelBox();
            comboBox.removeAllItems();
            for (String name : modelManager.getAllNames())
                comboBox.addItem(name);

            controlPanel.enableAll();
        }
    }

    @Override
    public void updateViewSelected() {
        if (controlPanel == null || currentModel == null)
            return;
        JCheckBox checkBox = controlPanel.getBoxViewSelected();
        if (checkBox.isSelected())
            currentModel.setColor(selectColor);
        else
            currentModel.reestabOriginalColor();
        display.repaint();
    }

    private void loadToDisplay(NamedModel model) {
        display.addModel(model);
    }
    @Override
    public void selectAddedModel() {
        String type = controlPanel.getBoxAppendModel().getSelectedItem().toString();
        if (type.equals(TYPE_BOX))
             new AppendBoxPanel(this, appendPanel);
        else if (type.equals(TYPE_TETRAHEDR))
            new AppendTetrahedrPanel(this, appendPanel);
        else if (type.equals(TYPE_CYLINDER))
            new AppendCylinderPanel(this, appendPanel);
        else if (type.equals(TYPE_SPHERE))
            new AppendSpherePanel(this, appendPanel);
        display.repaint();
    }

    @Override
    public void selectColor(Color color) {
        currentModel.setOriginalColor(color);
        currentModel.setColor(color);
        controlPanel.getBoxViewSelected().setSelected(false);
    }

    @Override
    public void disableAppendPanel() {
        appendPanel.setVisible(false);
    }


    @Override
    public void removeModel() {
        modelManager.removeModel(currentModel);
        display.removeModel(currentModel);
        if (modelManager.noModels())
            clearScene();
        else {
            currentModel = modelManager.getLast();
            update();
        }

    }

    @Override
    public void selectModel(String name) {
        if (currentModel != null)
            currentModel.reestabOriginalColor();

        currentModel = modelManager.getModel(name);
        if (controlPanel != null)
            controlPanel.getSelectModelBox().setSelectedItem(currentModel.getName());
        updateViewSelected();
        display.repaint();
    }

    @Override
    public Model getSelectedModel() {
        return currentModel;
    }

    @Override
    public void appendModel(NamedModel model) {
        if (modelManager.appendModel(model)) {
            updateComboBox();
            selectModel(model.getName());
            updateViewSelected();
            loadToDisplay(model);
            display.repaint();
        } else
            JOptionPane.showMessageDialog(null, "Такое имя уже существует");
    }

    @Override
    public void beginState() {
        headingSlider.setValue(180);
        pitchSlider.setValue(180);
        controlPanel.getSliderLight().setValue(0);
    }

    @Override
    public void clickDisplay(int x, int y) {
//        display.add2DShape(new Circle(new Vertex(x,y,0), 20));
//        disableAppendPanel();
    }

    @Override
    public void scaleModel() {
        JEditorPane edit = controlPanel.getEditorPane1();
        Double k = null;
        if (!edit.getText().equals(""))
            k = Double.parseDouble(edit.getText());
        if (k == null)
            k = 1.0;
        currentModel.scale(k);
        display.repaint();
    }
}