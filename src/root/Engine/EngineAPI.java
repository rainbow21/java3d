package root.Engine;

import root.Model.Model;
import root.Model.NamedModel;

import java.awt.*;

public interface EngineAPI {
    void update();
    void updateComboBox();
    void updateViewSelected();
    void removeModel();
    void selectModel(String name);
    Model getSelectedModel();

    void appendModel(NamedModel model);

    void beginState();

    void clearScene();
    void selectAddedModel();
    void selectColor(Color color);

    void disableAppendPanel();

    void clickDisplay(int x, int y);
    void scaleModel();
}
