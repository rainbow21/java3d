package root.Engine;

import root.ButtonClick.ButtonClick;
import root.ivDisplay;

import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class DisplayClick extends ButtonClick implements MouseListener {
    public DisplayClick(EngineAPI api, ivDisplay display) {
        super(api, display);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    }

    // Щелчек
    @Override
    public void mouseClicked(MouseEvent e) {

    }

    // Нажатие щелчок
    @Override
    public void mousePressed(MouseEvent e) {
        engineAPI.clickDisplay(e.getX(), e.getY());
        display.repaint();
    }


    // Отжатие кнопки
    @Override
    public void mouseReleased(MouseEvent e) {
    }

    // Курсор входит в область
    @Override
    public void mouseEntered(MouseEvent e) {

    }

    // Курсор выходит из области
    @Override
    public void mouseExited(MouseEvent e) {
    }
}
