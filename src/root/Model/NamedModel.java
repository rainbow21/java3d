package root.Model;

import root.Geometry.Triangle;

import java.util.ArrayList;

public class NamedModel extends Model {
    private String name;

    public NamedModel(String name) {
        super();
        this.name = name;
    }

    public NamedModel(Model m) {
        triangle = new ArrayList<>();
        for (Triangle t : m.triangle)
            triangle.add(t);
    }

    public String  getName() {
        return name;
    }
    public void rename(String newName) {
        name = newName;
    }
}
