package root.Model;

import root.Matrix3;
import root.Geometry.Triangle;
import root.Geometry.Vector;
import root.Geometry.Vertex;

import java.awt.*;
import java.beans.VetoableChangeListener;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;

public class Model implements ModelAPI {
    public static int stdSpeed = 50;
    protected ArrayList<Triangle> triangle;
    protected ArrayList<Triangle> originalTriangle;
    public Model() {
        triangle = new ArrayList<>();
        originalTriangle = new ArrayList<>();
    }

    @Override
    public void saveOriginal() {
        originalTriangle.clear();
        for (Triangle t : triangle)
            originalTriangle.add(new Triangle(t.getV1(), t.getV2(), t.getV3(), t.getOriginalColor()));
    }

    @Override
    public void reestabOriginal() {
        triangle.clear();
        for (Triangle t : originalTriangle)
            triangle.add(new Triangle(t.getV1(), t.getV2(), t.getV3(), t.getOriginalColor()));
    }


    public void moveOn(Vector v) {
        for (Triangle t : triangle) {
            t.getV1().moveOn(v);
            t.getV2().moveOn(v);
            t.getV3().moveOn(v);
        }

        for (Triangle t : originalTriangle) {
            t.getV1().moveOn(v);
            t.getV2().moveOn(v);
            t.getV3().moveOn(v);
        }
    }
    public void moveOn(double dx, double dy, double dz) {
        moveOn(new Vector(dx, dy, dz));
    }

    public void append(Triangle t) {
        triangle.add(new Triangle(t.getV1(), t.getV2(), t.getV3(), t.getColor()));
    }


    public ArrayList<Triangle> getTriangle() {
        return triangle;
    }
    public void setTriangle(ArrayList<Triangle> triangle) {
        this.triangle = triangle;
    }
    public void reestabOriginalColor() {
        for (Triangle t : triangle)
            t.reestabOriginalColor();
    }
    public void bigBoost(double size) {
        ArrayList<Triangle> buffer = new ArrayList<>();
        for (Triangle t : triangle) {
            Vertex m1 = new Vertex((t.getV1().x() + t.getV2().x())/2, (t.getV1().y() + t.getV2().y())/2, (t.getV1().z() + t.getV2().z())/2);
            Vertex m2 = new Vertex((t.getV2().x() + t.getV3().x())/2, (t.getV2().y() + t.getV3().y())/2, (t.getV2().z() + t.getV3().z())/2);
            Vertex m3 = new Vertex((t.getV1().x() + t.getV3().x())/2, (t.getV1().y() + t.getV3().y())/2, (t.getV1().z() + t.getV3().z())/2);

            buffer.add(new Triangle(t.getV1(), m1, m3, t.getColor()));
            buffer.add(new Triangle(t.getV2(), m2, m1, t.getColor()));
            buffer.add(new Triangle(t.getV3(), m3, m2, t.getColor()));
            buffer.add(new Triangle(m1, m2, m3, t.getColor()));
        }

        for (Triangle t : buffer)
            for (Vertex v : new Vertex[] {t.getV1(), t.getV2(), t.getV3()}) {
                double l = size / Math.sqrt(v.x()*v.x() + v.y()*v.y() + v.z()*v.z());
                v.mul(l);
            }
        triangle = buffer;
    }
    public void bigBoostK(int k, double size) {
        for (int i = 0; i < k; i++)
            bigBoost(size);
    }

    public void boost(Triangle t, int k) {
        triangle.remove(t);
        Vector normal = t.getNormal();

        // На 5 нормалей вверх
        normal.normalize();
        normal.mul(k);
        normal.negative();

        // Найдем центр треугольника
        Vertex center = new Vertex((
                t.getV1().x()+t.getV2().x()+t.getV3().x())/3,
                (t.getV1().y()+t.getV2().y()+t.getV3().y())/3,
                (t.getV1().z()+t.getV2().z()+t.getV3().z())/3);
        center.moveOn(normal);

        triangle.add(new Triangle(t.getV1(), t.getV2(), center, t.getColor()));
        triangle.add(new Triangle(center, t.getV2(), t.getV3(), t.getColor()));
        triangle.add(new Triangle(center, t.getV3(), t.getV1(), t.getColor()));
    }

    public void boostAll(int k) {
        ArrayList<Triangle> original = new ArrayList<>();
        for (Triangle t : triangle)
            original.add(t);

        for (Triangle t : original)
            boost(t, k);
    }

    public void saveToFile(String filename){
            try {
                Writer writer = new FileWriter(filename);
                for (Triangle t : triangle) {
                    Vertex v1 = t.getV1();
                    Vertex v2 = t.getV2();
                    Vertex v3 = t.getV3();
                    writer.write(String.format("%8.3f %8.3f %8.3f\n", v1.x(), v1.y(), v1.z()));
                    writer.write(String.format("%8.3f %8.3f %8.3f\n", v2.x(), v2.y(), v2.z()));
                    writer.write(String.format("%8.3f %8.3f %8.3f\n", v3.x(), v3.y(), v3.z()));
                    writer.write("\n");
                }
                writer.close();
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
    }

    @Override
    public Vertex getCenter() {
        Vertex sum = new Vertex(0,0,0);
        for (Triangle t : triangle) {
            sum.moveOn(t.getV1().x(), t.getV1().y(), t.getV1().z());
            sum.moveOn(t.getV2().x(), t.getV2().y(), t.getV2().z());
            sum.moveOn(t.getV3().x(), t.getV3().y(), t.getV3().z());
        }
        int n = triangle.size()*3;
        sum.mul(1.0/n);
        return sum;
    }

    @Override
    public double getFarDistance() {
        double p = 0;
        Vertex center = getCenter();
        for (Triangle t : triangle) {
            double len1 = new Vector(center, t.getV1()).getLength();
            double len2 = new Vector(center, t.getV2()).getLength();
            double len3 = new Vector(center, t.getV3()).getLength();
            p = Math.max(p, Math.max(len1, Math.max(len2, len3)));
        }

        return p;
    }

    public Vertex getOriginalCenter() {
        Vertex sum = new Vertex(0,0,0);
        for (Triangle t : originalTriangle) {
            sum.moveOn(t.getV1().x(), t.getV1().y(), t.getV1().z());
            sum.moveOn(t.getV2().x(), t.getV2().y(), t.getV2().z());
            sum.moveOn(t.getV3().x(), t.getV3().y(), t.getV3().z());
        }
        int n = originalTriangle.size()*3;
        sum.mul(1.0/n);
        return sum;
    }



    @Override
    public void moveUp(int d) {
        moveOn(0, d, 0);
    }
    @Override
    public void moveDown(int d) {
        moveOn(0, -d, 0);
    }
    @Override
    public void moveLeft(int d) {
        moveOn(d, 0, 0);
    }
    @Override
    public void moveRight(int d) {
        moveOn(-d, 0, 0);
    }

    @Override
    public void moveForward(int d) {
        moveOn(0,0,-d);
    }

    @Override
    public void moveBack(int d) {
        moveOn(0,0,d);
    }

    @Override
    public void setColor(Color color) {
        for (Triangle t : triangle)
            t.setColor(color);
    }

    @Override
    public void setOriginalColor(Color color) {
        for (Triangle t : triangle)
            t.setOriginalColor(color);
    }

    public void transform(Matrix3 matrix) {
        for (Triangle t : triangle)
            t.transform(matrix);
    }

    private void rotationXRelative0(double angle) {
        for (Triangle t : triangle)
            t.transform(Matrix3.rotationX(angle));
        for (Triangle t : originalTriangle)
            t.transform(Matrix3.rotationX(angle));
    }

    @Override
    public void rotationX(double angle) {
        Vertex center = getCenter();
        moveOn(-center.x(), -center.y(), -center.z());
        rotationXRelative0(angle);
        moveOn(center.x(), center.y(), center.z());
    }

    private void rotationYRelative0(double angle) {
        for (Triangle t : triangle)
            t.transform(Matrix3.rotationY(angle));
        for (Triangle t : originalTriangle)
            t.transform(Matrix3.rotationY(angle));
    }

    @Override
    public void rotationY(double angle) {
        Vertex center = getCenter();
        moveOn(-center.x(), -center.y(), -center.z());
        rotationYRelative0(angle);
        moveOn(center.x(), center.y(), center.z());
    }

    @Override
    public void scale(double k) {
        Vertex center = getCenter();
        moveOn(-center.x(), -center.y(), -center.z());
        scaleRelative0(k);
        moveOn(center.x(), center.y(), center.z());
    }

    private void scaleRelative0(double k) {
        for (Triangle t : triangle)
            t.transform(Matrix3.scale(k));
        for (Triangle t : originalTriangle)
            t.transform(Matrix3.scale(k));
    }
}
