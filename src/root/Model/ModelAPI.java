package root.Model;

import root.Geometry.Vector;
import root.Geometry.Vertex;

import java.awt.*;

public interface ModelAPI {
    void moveUp(int d);
    void moveDown(int d);
    void moveLeft(int d);
    void moveRight(int d);
    void moveForward(int d);
    void moveBack(int d);

    void setColor(Color color);
    void setOriginalColor(Color color);

    void rotationX(double angle);
    void rotationY(double angle);

    void saveOriginal();
    void reestabOriginal();

    void scale(double k);

    Vertex getCenter();
    double getFarDistance();
    void moveOn(Vector v);
    void moveOn(double dx, double dy, double dz);
}
