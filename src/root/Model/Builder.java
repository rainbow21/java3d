package root.Model;

import root.Geometry.CylinderData;
import root.Geometry.Triangle;
import root.Geometry.Vertex;

import java.awt.*;
import java.util.ArrayList;

import static java.lang.Math.abs;

public class Builder {
    private static Model buildBox(int size) {
        Model result = new Model();

        Vertex v1 = new Vertex(0, 0,    0);
        Vertex v2 = new Vertex(size, 0,    0);
        Vertex v3 = new Vertex(size,    size, 0);
        Vertex v4 = new Vertex(0,    size, 0);

        Vertex v5 = new Vertex(0, 0,    size);
        Vertex v6 = new Vertex(size, 0,    size);
        Vertex v7 = new Vertex(size,    size, size);
        Vertex v8 = new Vertex(0,    size, size);

        result.append(new Triangle(v1, v3, v4, Color.WHITE));
        result.append(new Triangle(v1, v2, v3, Color.WHITE));

        result.append(new Triangle(v7, v4, v3, Color.YELLOW));
        result.append(new Triangle(v7, v8, v4, Color.YELLOW));

        result.append(new Triangle(v2, v7, v3, Color.GREEN));
        result.append(new Triangle(v2, v6, v7, Color.GREEN));

        result.append(new Triangle(v1, v4, v8, Color.RED));
        result.append(new Triangle(v1, v8, v5, Color.RED));

        result.append(new Triangle(v1, v5, v6, Color.BLUE));
        result.append(new Triangle(v1, v6, v2, Color.BLUE));

        result.append(new Triangle(v6, v8, v7, Color.CYAN));
        result.append(new Triangle(v6, v5, v8, Color.CYAN));
        return result;
    }
    private static Model buildTetrahedor(int size) {
        Model result = new Model();
        result.append(new Triangle(
                new Vertex(size, size, size),
                new Vertex(-size, -size, size),
                new Vertex(-size, size, -size),
                Color.WHITE
        ));
        result.append(new Triangle(
                new Vertex(size, size, size),
                new Vertex(size, -size, -size),
                new Vertex(-size, -size, size),
                Color.RED
        ));
        result.append(new Triangle(
                new Vertex(-size, size, -size),
                new Vertex(size, -size, -size),
                new Vertex(size, size, size),
                Color.GREEN
        ));

        result.append(new Triangle(
                new Vertex(-size, size, -size),
                new Vertex(-size, -size, size),
                new Vertex(size, -size, -size),
                Color.BLUE
        ));
        return result;
    }
    private static Model buildCircle(double radius, int k) {
        ArrayList<Vertex> list = new ArrayList<>();
        double dAlpha = 360.0 / k;

        for (int i = 0; i < k; i++) {
            double tAlpha = i * dAlpha;
            double x = radius * Math.cos(Math.toRadians(tAlpha));
            double y = radius * Math.sin(Math.toRadians(tAlpha));
            list.add(new Vertex(x, y, 0));
        }

        Model result = new Model();
        int n = list.size();
        for (int i = 0; i < n-1; i++)
            result.append(new Triangle(list.get(i), list.get(i+1), new Vertex(0,0,0), Color.RED));
        result.append(new Triangle(list.get(n-1), list.get(0), new Vertex(0,0,0), Color.RED));



        return result;
    }
    private static Model buildСylinder(Model base, double h) {
        Model result = new Model();
        for (Triangle t : base.getTriangle())
            result.append(t);

        // V3 в каждом треугольнике это центр. V1,V2 - края
        for (Triangle t : base.getTriangle()) {
            Vertex v1 = new Vertex(t.getV1());
            Vertex v2 = new Vertex(t.getV2());
            Vertex v3 = new Vertex(t.getV2());
            v3.moveOn(0,0,h);
            result.append(new Triangle(v2, v1, v3, Color.RED));

            v2 = new Vertex(t.getV1());
            v2.moveOn(0,0,h);
            result.append(new Triangle(v1, v2, v3, Color.YELLOW));
        }

        base.moveOn(0,0,h);
        for (Triangle t : base.getTriangle()) {
            t.reverse();
            t.setColor(Color.YELLOW);
            result.append(t);
        }


        return result;
    }

    public static NamedModel buildBox(String name, int size) {
        if (size == 0)
            size++;
        NamedModel result = new NamedModel(buildBox(abs(size)));
        result.rename(name);
        result.saveOriginal();
        return result;
    }

    public static NamedModel buildTetrahedor(String name, int size) {
        if(size == 0)
            size++;
        NamedModel result = new NamedModel(buildTetrahedor(abs(size)));
        result.rename(name);
        result.saveOriginal();
        return result;
    }

    public static NamedModel buildCylinder(String name, CylinderData data) {
        if (data.radius == 0)
            data.radius++;
        if (data.h == 0)
            data.h++;
        if (abs(data.k) < 3)
            data.k = 3;
        NamedModel result = new NamedModel(buildСylinder(buildCircle(abs(data.radius), abs(data.k)), abs(data.h)));
        result.rename(name);
        result.saveOriginal();
        return result;
    }

    public static NamedModel buildSphere(String name, int size, int boost) {
        NamedModel result = new NamedModel(buildTetrahedor(abs(size)));
        result.rename(name);
        result.bigBoostK(abs(boost), size);
        result.saveOriginal();
        return result;
    }
}
