package root.Model;

import java.util.ArrayList;

public class Manager {

    ArrayList<NamedModel> allModel;
    public Manager() {
        allModel = new ArrayList<>();
    }

    public void removeAllModel() {
        allModel.clear();
    }

    public boolean noModels() {
        return allModel.isEmpty();
    }

    public String[] getAllNames() {
        int n = allModel.size();
        String str[] = new String[n];
        for (int i = 0; i < n; i++)
            str[i] = allModel.get(i).getName();

        return str;
    }

    public boolean appendModel(NamedModel model) {
        String names[] = getAllNames();
        int n = names.length;
        for (int i = 0; i < n; i++)
            if (allModel.get(i).getName().equals(model.getName()))
                return false;

        allModel.add(model);
        return true;
    }

    public NamedModel getLast() {
        return allModel.get(allModel.size()-1);
    }

    public NamedModel getModel(String name) {
        for (NamedModel model : allModel)
            if (model.getName().equals(name))
                return  model;
        return null;
    }

    public void removeModel(NamedModel model) {
        allModel.remove(model);
    }
}
