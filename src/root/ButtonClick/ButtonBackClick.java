package root.ButtonClick;

import root.Engine.EngineAPI;
import root.Model.Model;
import root.ivDisplay;

import java.awt.event.ActionEvent;

public class ButtonBackClick extends ButtonClick {
    public ButtonBackClick(EngineAPI api, ivDisplay display) {
        super(api, display);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        engineAPI.getSelectedModel().moveBack(Model.stdSpeed);
        display.repaint();
    }
}
