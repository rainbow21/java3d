package root.ButtonClick;

import root.Engine.EngineAPI;
import root.ivDisplay;

import java.awt.event.ActionEvent;

public class ButtonRemoveModelClick extends ButtonClick {
    public ButtonRemoveModelClick(EngineAPI api, ivDisplay display) {
        super(api, display);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        engineAPI.removeModel();
        display.repaint();
    }
}
