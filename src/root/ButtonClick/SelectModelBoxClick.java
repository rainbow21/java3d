package root.ButtonClick;

import root.Engine.EngineAPI;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SelectModelBoxClick implements ActionListener {
    private EngineAPI engineAPI;
    public SelectModelBoxClick(EngineAPI api) {
        engineAPI = api;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        JComboBox box = (JComboBox)e.getSource();
        if (box.getItemCount() != 0)
            engineAPI.selectModel(box.getSelectedItem().toString());
    }
}
