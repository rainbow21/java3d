package root.ButtonClick;

import root.Engine.EngineAPI;
import root.ivDisplay;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class ButtonColorClick extends ButtonClick {
    public ButtonColorClick(EngineAPI api, ivDisplay display) {
        super(api, display);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Color color = JColorChooser.showDialog(null, "Цвет модели", null);
        engineAPI.selectColor(color);
        display.repaint();
    }
}
