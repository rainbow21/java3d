package root.ButtonClick;

import root.Engine.EngineAPI;
import root.Geometry.Vector;
import root.ivDisplay;

import java.awt.event.ActionEvent;

public class ButtonLightDownClick extends ButtonClick {
    public ButtonLightDownClick(EngineAPI api, ivDisplay display) {
        super(api, display);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        display.lightMoveTo(ivDisplay.LIGHT_TO_DOWN);
        display.repaint();
    }
}
