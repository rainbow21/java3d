package root.ButtonClick;

import root.Engine.EngineAPI;
import root.ivDisplay;

import java.awt.event.ActionEvent;

public class ButtonXUpRotation extends ButtonClick {
    public ButtonXUpRotation(EngineAPI api, ivDisplay display) {
        super(api, display);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        engineAPI.getSelectedModel().rotationX(-Math.PI/8);
        display.repaint();
    }
}
