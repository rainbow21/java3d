package root.ButtonClick;

import root.Engine.EngineAPI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BoxViewSelectedClick implements ActionListener {
    private EngineAPI engineAPIAPI;
    public BoxViewSelectedClick(EngineAPI api) {
        engineAPIAPI = api;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        engineAPIAPI.updateViewSelected();
    }
}
