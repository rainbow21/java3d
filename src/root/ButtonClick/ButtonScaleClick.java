package root.ButtonClick;

import root.Engine.EngineAPI;
import root.ivDisplay;

import java.awt.event.ActionEvent;

public class ButtonScaleClick extends ButtonClick {
    public ButtonScaleClick(EngineAPI api, ivDisplay display) {
        super(api, display);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        engineAPI.scaleModel();
        display.repaint();
    }
}
