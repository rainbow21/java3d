package root.ButtonClick;

import root.Engine.EngineAPI;
import root.Geometry.Vector;
import root.ivDisplay;

import java.awt.event.ActionEvent;

public class ButtonLightStdClick extends ButtonClick {
    public ButtonLightStdClick(EngineAPI api, ivDisplay display) {
        super(api, display);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        display.lightMoveTo(ivDisplay.LIGHT_TO_STD);
        display.repaint();
    }
}
