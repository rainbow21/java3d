package root.ButtonClick;

import root.Engine.EngineAPI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AppendModelBoxClick  implements ActionListener {
    private EngineAPI engineAPI;

    public AppendModelBoxClick(EngineAPI api) {
        this.engineAPI = api;
    }


    int count = 1;
    @Override
    public void actionPerformed(ActionEvent e) {
        if (count != 1) {
            engineAPI.disableAppendPanel();
            engineAPI.selectAddedModel();
        }
        count = 2;
    }
}
