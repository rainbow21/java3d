package root.ButtonClick;

import root.Engine.EngineAPI;
import root.ivDisplay;

import java.awt.event.ActionEvent;

public class ButtonBeginStateClick extends ButtonClick {
    public ButtonBeginStateClick(EngineAPI api, ivDisplay display) {
        super(api, display);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        engineAPI.beginState();
        display.repaint();
    }
}
