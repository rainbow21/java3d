package root.ButtonClick;

import root.Engine.EngineAPI;
import root.ivDisplay;

import java.awt.event.ActionEvent;

public class ButtonReestabOrigialClick extends ButtonClick {
    public ButtonReestabOrigialClick(EngineAPI api, ivDisplay display) {
        super(api, display);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        display.removeModel(engineAPI.getSelectedModel());
        engineAPI.getSelectedModel().reestabOriginal();
        display.addModel(engineAPI.getSelectedModel());
        engineAPI.updateViewSelected();
        display.repaint();
    }
}
