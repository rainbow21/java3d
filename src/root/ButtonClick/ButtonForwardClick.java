package root.ButtonClick;

import root.Engine.EngineAPI;
import root.Model.Model;
import root.ivDisplay;

import java.awt.event.ActionEvent;

public class ButtonForwardClick extends ButtonClick {
    public ButtonForwardClick(EngineAPI api, ivDisplay display) {
        super(api, display);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        engineAPI.getSelectedModel().moveForward(Model.stdSpeed);
        display.repaint();
    }
}
