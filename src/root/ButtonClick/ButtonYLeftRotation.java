package root.ButtonClick;

import root.Engine.EngineAPI;
import root.ivDisplay;

import java.awt.event.ActionEvent;

public class ButtonYLeftRotation extends ButtonClick {
    public ButtonYLeftRotation(EngineAPI api, ivDisplay display) {
        super(api, display);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        engineAPI.getSelectedModel().rotationY(Math.PI/8);
        display.repaint();
    }
}
