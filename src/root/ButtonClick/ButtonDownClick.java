package root.ButtonClick;

import root.Engine.EngineAPI;
import root.Model.Model;
import root.Model.ModelAPI;
import root.ivDisplay;

import java.awt.event.ActionEvent;

public class ButtonDownClick extends ButtonClick {
    public ButtonDownClick(EngineAPI api, ivDisplay display) {
        super(api, display);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        engineAPI.getSelectedModel().moveDown(Model.stdSpeed);
        display.repaint();
    }
}
