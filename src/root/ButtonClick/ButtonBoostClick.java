package root.ButtonClick;

import root.Engine.EngineAPI;
import root.ivDisplay;

import java.awt.event.ActionEvent;

public class ButtonBoostClick extends ButtonClick {
    public ButtonBoostClick(EngineAPI api, ivDisplay display) {
        super(api, display);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        display.removeModel(engineAPI.getSelectedModel());
        engineAPI.getSelectedModel().reestabOriginalColor();
        double dist = engineAPI.getSelectedModel().getFarDistance();
        engineAPI.getSelectedModel().bigBoost(dist);
        display.addModel(engineAPI.getSelectedModel());
        engineAPI.updateViewSelected();
        display.repaint();
    }
}
