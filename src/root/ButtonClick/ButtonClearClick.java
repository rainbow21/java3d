package root.ButtonClick;

import root.Engine.Engine;
import root.Engine.EngineAPI;
import root.Model.Model;
import root.ivDisplay;

import java.awt.event.ActionEvent;

public class ButtonClearClick extends ButtonClick {
    public ButtonClearClick(EngineAPI api, ivDisplay display) {
        super(api, display);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        engineAPI.clearScene();
        display.repaint();
    }
}
