package root.ButtonClick;

import root.Engine.EngineAPI;
import root.Model.Model;
import root.ivDisplay;

import java.awt.event.ActionEvent;

public class ButtonLeftClick extends ButtonClick {
    public ButtonLeftClick(EngineAPI api, ivDisplay display) {
        super(api, display);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        engineAPI.getSelectedModel().moveLeft(Model.stdSpeed);
        display.repaint();
    }
}
