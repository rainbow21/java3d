package root.Geometry;

public class Vector extends Vertex {
    public Vector(double x, double y, double z) {
        super(x, y, z);
    }
    public Vector(Vertex v1, Vertex v2) {
        super(v2.x-v1.x, v2.y-v1.y, v2.z-v1.z);
    }


    public double getLength() {
        return Math.sqrt(x*x + y*y + z*z);
    }
    public void mul(double k) {
        x *= k;
        y *= k;
        z *= k;
    }
    public void normalize() {
        double len = getLength();
        x /= len;
        y /= len;
        z /= len;
    }

    public Vector vectorMul(Vector v) {
//        return new Vector(x*v.z - z*v.y, z*v.x - x*v.z, x*v.y - y*v.x);
        return new Vector(y*v.z - z*v.y, z*v.x - x*v.z, x*v.y - y*v.x);
    }

    public void negative() {
        mul(-1.0);
    }
}
