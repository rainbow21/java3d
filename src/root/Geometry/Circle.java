package root.Geometry;

import root.ivDisplay;

public class Circle implements DrawableShape {
    private double radius;
    private Vertex center;

    public Circle(Vertex center, double radius) {
        this.radius = radius;
        this.center = center;
    }

    @Override
    public void draw(ivDisplay display) {
        display.drawCircle(center, (int)radius);
    }
}
