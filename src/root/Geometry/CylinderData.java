package root.Geometry;

public class CylinderData {
    public int h;
    public int radius;
    public int k;
    public CylinderData(int radius, int k, int h) {
        this.h = h;
        this.radius = radius;
        this.k = k;
    }
}
