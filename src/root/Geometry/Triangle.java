package root.Geometry;

import root.Matrix3;

import java.awt.*;

public class Triangle {
    private Vertex v1;
    private Vertex v2;
    private Vertex v3;
    Color color;
    Color originalColor;
    public Triangle(Vertex v1, Vertex v2, Vertex v3, Color color) {
        this.v1 = new Vertex(v1);
        this.v2 = new Vertex(v2);
        this.v3 = new Vertex(v3);
        this.color = color;
        this.originalColor = color;
    }

    public Triangle(Vertex v1, Vertex v2, Vertex v3) {
        this.v1 = new Vertex(v1);
        this.v2 = new Vertex(v2);
        this.v3 = new Vertex(v3);
        this.color = Color.WHITE;
        this.originalColor = Color.WHITE;
    }

    public Vertex getV1() {
        return v1;
    }
    public Vertex getV2() {
        return v2;
    }
    public Vertex getV3() {
        return v3;
    }
    public Color getColor() {
        return color;
    }
    public void setColor(Color color) {
        this.color = color;
    }
    public Color getOriginalColor() {
        return originalColor;
    }
    public void reestabOriginalColor() {
        color = originalColor;
    }
    public void setOriginalColor(Color color) {
        originalColor = color;
    }
    public Vertex getCenter() {
        return new Vertex((v1.x + v2.x + v3.x)/3.0, (v1.z + v2.z + v3.z)/3, (v1.z + v2.z + v3.z)/3);
    }
    public void reverse() {
        Vertex p = v2;
        v2 = v3;
        v3 = p;
    }
    public Vector getNormal() {
        Vector ab = new Vector(v2.x - v1.x, v2.y - v1.y, v2.z - v1.z);
        Vector ac = new Vector(v3.x - v1.x, v3.y - v1.y, v3.z - v1.z);

        return ab.vectorMul(ac);
    }

    public void transform(Matrix3 matrix) {
        v1 = matrix.transform(v1);
        v2 = matrix.transform(v2);
        v3 = matrix.transform(v3);
    }
}
