package root.Geometry;

import root.ivDisplay;

public class Line implements DrawableShape {
    private double x1, y1;
    private double x2, y2;
    public Line(double x1, double y1, double x2, double y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    public Line(Vertex v1, Vertex v2) {
        this.x1 = v1.x;
        this.y1 = v1.y;

        this.x2 = v2.x;
        this.y2 = v2.y;
    }

    public double getX1() {
        return x1;
    }

    public double getY1() {
        return y1;
    }

    public double getX2() {
        return x2;
    }

    public double getY2() {
        return y2;
    }

    @Override
    public void draw(ivDisplay display) {
        display.drawLine(this);
    }
}
