package root.Geometry;

public class Vertex {
    protected double x;
    protected double y;
    protected double z;


    public Vertex(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public Vertex(Vertex v) {
        this.x = v.x;
        this.y = v.y;
        this.z = v.z;
    }

    public double x() {
        return x;
    }
    public double y() {
        return y;
    }
    public double z() {
        return z;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public void moveOn(double dx, double dy, double dz) {
        x += dx;
        y += dy;
        z += dz;
    }
    public void moveOn(Vector dv) {
        moveOn(dv.x, dv.y, dv.z);
    }

    public void mul(double k) {
        x *= k;
        y *= k;
        z *= k;
    }

    public void mul(Vector v) {
        x *= v.x;
        y *= v.y;
        z *= v.z;
    }
}
