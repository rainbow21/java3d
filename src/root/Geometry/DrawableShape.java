package root.Geometry;

import root.ivDisplay;

public interface DrawableShape {
    void draw(ivDisplay display);
}
