package root;

import root.Geometry.Vertex;

public class Matrix3 {
    double values[];
    final int N = 9;
    Matrix3(double values[]) {
        this.values =values;
    }

    Matrix3 multiply(Matrix3 other) {
        double result[] = new double [N];
        for (int row = 0; row < 3; row++)
            for (int col = 0; col < 3; col++)
                for (int i = 0; i < 3; i++)
                    result[row*3 + col] += this.values[row*3 + i] * other.values[i*3 + col];

        return new Matrix3(result);
    }

    public Vertex transform(Vertex in) {
        return new Vertex(
                in.x()*values[0] + in.y()*values[3] + in.z()*values[6],
                in.x()*values[1] + in.y()*values[4] + in.z()*values[7],
                in.x()*values[2] + in.y()*values[5] + in.z()*values[8]
                );
    }

    public static Matrix3 rotationY(double angle) {
        return new Matrix3(new double[] {
                Math.cos(angle), 0, -Math.sin(angle),
                0, 1, 0,
                Math.sin(angle), 0, Math.cos(angle)
        });
    }

    public static Matrix3 single() {
        return new Matrix3(new double[] {
           1, 0, 0,
           0, 1, 0,
           0, 0, 1
        });
    }
    public static Matrix3 rotationX(double angle) {
        return new Matrix3(new double[] {
                1, 0, 0,
                0, Math.cos(angle), Math.sin(angle),
                0, -Math.sin(angle), Math.cos(angle)
        });
    }

    public static Matrix3 scale(double k) {
        return new Matrix3(new double[] {
                k, 0, 0,
                0, k, 0,
                0, 0, k
        });
    }

    public static Matrix3 perpective(double k) {
        double r = k/1000;
        return new Matrix3(new double[] {
                1/r, 0, 0,
                0, 1/r, 0,
                0, 0, 1
        });
    }
}
